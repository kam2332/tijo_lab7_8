package pl.edu.pwsztar.domain.files;

import org.springframework.core.io.InputStreamResource;
import pl.edu.pwsztar.domain.dto.FileDto;

public interface CsvGenerator {

    InputStreamResource toCsv(FileDto fileDto);
}
