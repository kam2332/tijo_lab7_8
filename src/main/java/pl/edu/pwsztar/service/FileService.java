package pl.edu.pwsztar.service;

import org.springframework.core.io.InputStreamResource;
import pl.edu.pwsztar.domain.dto.FileDto;

import java.io.IOException;
import java.util.List;

public interface FileService {

    List<FileDto> getMoviesSorted();

    InputStreamResource writeMovies(List<FileDto> fileDtoList) throws IOException;
}
