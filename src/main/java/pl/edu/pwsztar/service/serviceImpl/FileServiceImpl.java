package pl.edu.pwsztar.service.serviceImpl;

import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.converter.Converter;
import pl.edu.pwsztar.domain.dto.FileDto;
import pl.edu.pwsztar.domain.entity.Movie;
import pl.edu.pwsztar.domain.files.TxtGenerator;
import pl.edu.pwsztar.domain.repository.MovieRepository;
import pl.edu.pwsztar.service.FileService;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class FileServiceImpl implements FileService, TxtGenerator {
    private final MovieRepository movieRepository;
    private final Converter<List<Movie>, List<FileDto>> converter;

    public FileServiceImpl(MovieRepository movieRepository, Converter<List<Movie>, List<FileDto>> converter) {
        this.converter = converter;
        this.movieRepository = movieRepository;
    }

    @Override
    public List<FileDto> getMoviesSorted() {
        List<Movie> movies = movieRepository.findAll(Sort.by("year").descending());
        return converter.convert(movies);
    }

    @Override
    public InputStreamResource writeMovies(List<FileDto> fileDtoList) throws IOException {
        List<InputStream> inputStreamList = new ArrayList<>();
        for (FileDto fileDto : fileDtoList) {
            inputStreamList.add(toTxt(fileDto).getInputStream());
        }
        return new InputStreamResource(new SequenceInputStream(Collections.enumeration(inputStreamList)));
    }


    @Override
    public InputStreamResource toTxt(FileDto fileDto) throws IOException {
        File file = File.createTempFile("tmp", ".txt");
        FileOutputStream fileOutputStream = new FileOutputStream(file);

        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(fileOutputStream));
        bufferedWriter.write(fileDto.getYear() + " " + fileDto.getTitle());
        bufferedWriter.newLine();

        bufferedWriter.close();

        fileOutputStream.flush();
        fileOutputStream.close();

        InputStream stream = new FileInputStream(file);
        return new InputStreamResource(stream);
    }
}
